from django.contrib import admin
from hello2.models import Article, Author

admin.site.register(Article)
admin.site.register(Author)
