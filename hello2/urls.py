from django.conf.urls import patterns, url
from hello2 import views

urlpatterns = patterns('',
    url(r'^articles/', views.ArticleList.as_view()),
)
