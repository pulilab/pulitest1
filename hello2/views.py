from django.views.generic import ListView
from hello2.models import Article


class ArticleList(ListView):
    model = Article
    template_name = 'article_list.html'
