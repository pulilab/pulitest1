from django.views.generic import TemplateView


class Page1View(TemplateView):
    template_name = 'page1.html'

class Page2View(TemplateView):
    template_name = 'page2.html'

class Page3View(TemplateView):
    template_name = 'page3.html'