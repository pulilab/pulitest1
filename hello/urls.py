from django.conf.urls import patterns, url
from hello import views

urlpatterns = patterns('',
    url(r'^page1/', views.Page1View.as_view()),
    url(r'^page2/', views.Page2View.as_view()),
    url(r'^page3/', views.Page3View.as_view()),
)
